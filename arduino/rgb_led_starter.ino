//pin values
//DSB

#define BRIGHTNESS 500

int redPin = 4;
int greenPin = 3;
int bluePin = 2;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}
// set n and count
int n = 0;
int count;

void loop() {
  setColor(random(255),random(255),random(255));
  delay(1000);
  //setColor(75,0,130);
  //delay(1000); 
  //setColor(255, n, 0);
  //delay(1000);
  //setColor(255,105,180);
  //delay(1000);
  //setColor(210,105,30);
  //delay(1000); 
   Serial.print(n);
   Serial.print(" ");
   count++;
  if (count % 20 == 0)Serial.println();
  
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
