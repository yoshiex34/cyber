// RGBW (Red Green Blue White Neo-Pixel starter code
// 16 LEDS
// Daniel Brogan

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#define PIN 6
#define NUM_LEDS 16
#define BRIGHTNESS 150

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

// Initialize some variables for the void loop()
int led1,led2,led3, red, green, blue;
int wait = 5;
int white = 0;
void loop() {
// turn on leds 
  led1 = random(16);
  led2 = led1 % 3;
  led3 =  led1 % 4;
  red = random(130,0);
  green = random(0,50);
  blue = random(130,0);
  //white = random(0,10);
  white = 0;

    strip.setPixelColor(led1, red, green , blue, white);
    strip.setPixelColor(led2, red, green , blue, white);
    strip.setPixelColor(led3, red, green , blue, white);
    strip.show();
   delay(wait);
   //this loop sets all leds to black
   for ( led1 = 0; led1 < 16; led1++){  
    strip.setPixelColor(led1, 0,0,0,0);
  }//end of for loop
    strip.show();


}
